package com.nortal.services;

import com.nortal.exceptions.BookNotFoundException;
import com.nortal.model.Book;
import com.nortal.model.Borrower;
import com.nortal.model.Comment;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class BooksService
{
	private ArrayList<Book> books = new ArrayList<Book>();

	public BooksService()
	{
		this.fillBooks();
	}

	public Collection<Book> getBooks()
	{
		return this.books;
	}

	public Book getByBookId( final int bookId )
	{
		Book requestedBook = null;

		for ( Book book : this.books )
		{
			if ( bookId == book.getId() )
			{
				requestedBook = book;
				break;
			}
		}

		if ( null == requestedBook )
		{
			throw new BookNotFoundException( bookId );
		}

		return requestedBook;
	}

	private void fillBooks()
	{
		// @todo Book1
		Book book1 = new Book( 1 );

		book1
			.setTitle( "ActiveMQ in Action" )
			.setQuantity( 1 )
			.setAvailable( 0 )
			.setRating( 10 )
			.setLocation( "Tartu" )
			.setPublishingYear( 2011 )

			.addAuthor( "Bruce Snyder" )
			.addAuthor( "Dejan Bosanac" )
			.addAuthor( "Rob Davies" )

			.addTag( "ActiveMQ" )
			.addTag( "Java" );

		Borrower book1Borrower1 = new Borrower( "Pille Kadakas" );
		book1Borrower1
			.setFrom( "2014-01-31T10:35:09Z" )
			.setTo( "2014-02-10T10:35:09Z" );
		
		book1.addBorrower( book1Borrower1 );
		this.books.add( book1 );

		// @todo Book2
		Book book2 = new Book( 2 );

		book2
			.setTitle( "Algorithms of the Intelligent Web" )
			.setQuantity( 3 )
			.setAvailable( 0 )
			.setRating( 10 )
			.setLocation( "Tartu" )
			.setPublishingYear( 2011 )

			.addAuthor( "Haralambos Marmanis" )
			.addAuthor( "Dmitry Babenko" )

			.addTag( "Algorithms" )
			.addTag( "Java" );

		Borrower book2Borrower1 = new Borrower( "Juhan Juurikas" );
		book2Borrower1
			.setFrom( "2014-02-26T07:20:12Z" )
			.setTo( "2014-03-08T07:20:12Z" );
		book2.addBorrower( book2Borrower1 );

		Borrower book2Borrower2 = new Borrower( "Pille Kadakas" );
		book2Borrower2
			.setFrom( "2014-03-08T07:20:12Z" )
			.setTo( "2014-01-27T14:17:24Z" );
		book2.addBorrower( book2Borrower2 );

		Borrower book2Borrower3 = new Borrower( "Juhan Juurikas" );
		book2Borrower1
			.setFrom( "2014-02-23T07:20:12Z" )
			.setTo( "2014-03-05T07:20:12Z" );
		book2.addBorrower( book2Borrower3 );

		Comment book2Comment1 = new Comment( "Too boring book!" );
		book2Comment1
			.setAuthor( "Juhan Juurikas" )
			.setRating( 1 )
			.setDate( "2014-02-15T14:17:24Z" );
		book2.addComment( book2Comment1 );
		this.books.add( book2 );

		// @todo Book3
		Book book3 = new Book( 3 );

		book3
			.setTitle( "Art of Java Web Development" )
			.setQuantity( 3 )
			.setAvailable( 0 )
			.setRating( 6 )
			.setLocation( "Tallinn" )
			.setPublishingYear( 2001 )

			.addAuthor( "Neal Ford" )

			.addTag( "Web Development" )
			.addTag( "Java" );

		Comment book3Comment1 = new Comment( "Too boring book!" );
		book3Comment1
			.setAuthor( "Pille Kadakas" )
			.setRating( 3 )
			.setDate( "2014-02-15T14:17:24Z" );
		book3.addComment( book3Comment1 );
		this.books.add( book3 );

		// @todo Book4
		Book book4 = new Book( 4 );

		book4
			.setTitle( "AspectJ in Action" )
			.setQuantity( 3 )
			.setAvailable( 1 )
			.setRating( 1 )
			.setLocation( "Tartu" )
			.setPublishingYear( 2004 )

			.addAuthor( "Ramnivas Laddad" )
			
			.addTag( "AspectJ" )
			.addTag( "Java" );

		Comment book4Comment1 = new Comment( "Fantastic book!" );
		book4Comment1
			.setAuthor( "Juhan Juurikas" )
			.setRating( 5 )
			.setDate( "2014-02-15T14:17:24Z" );
		book4.addComment( book4Comment1 );
		this.books.add( book4 );

		// @todo Book5
		Book book5 = new Book( 5 );

		book5
			.setTitle( "Bitter Java" )
			.setQuantity( 2 )
			.setAvailable( 0 )
			.setRating( 1 )
			.setLocation( "Tartu" )
			.setPublishingYear( 2007 )

			.addAuthor( "Bruce A. Tate" )

			.addTag( "Java" );

		Borrower book5Borrower1 = new Borrower( "Pille Kadakas" );
		book5Borrower1
			.setFrom( "2014-02-11T07:20:12Z" )
			.setTo( "2014-02-21T07:20:12Z" );
		book5.addBorrower( book5Borrower1 );

		Comment book5Comment1 = new Comment( "Fantastic book!" );
		book5Comment1
			.setAuthor( "Juhan Juurikas" )
			.setRating( 7 )
			.setDate( "2014-02-15T14:17:24Z" );
		book5.addComment( book5Comment1 );
		this.books.add( book5 );

		// @todo Book6
		Book book6 = new Book( 6 );

		book6
			.setTitle( "Camel in Action" )
			.setQuantity( 2 )
			.setAvailable( 0 )
			.setRating( 8 )
			.setLocation( "Tallinn" )
			.setPublishingYear( 2010 )

			.addAuthor( "Claus Ibsen" )
			.addAuthor( "Jonathan Anstey" )

			.addTag( "Camel" )
			.addTag( "Java" );

		Comment book6Comment1 = new Comment( "Fantastic book!" );
		book6Comment1
			.setAuthor( "Juhan Juurikas" )
			.setRating( 7 )
			.setDate( "2014-02-15T14:17:24Z" );
		book6.addComment( book6Comment1 );
		this.books.add( book6 );

		// @todo Book7
		Book book7 = new Book( 7 );

		book7
			.setTitle( "Distributed Programming with Java" )
			.setQuantity( 1 )
			.setAvailable( 0 )
			.setRating( 8 )
			.setLocation( "Tallinn" )
			.setPublishingYear( 2004 )

			.addAuthor( "Qusay H. Mahmoud" )

			.addTag( "Distributed" )
			.addTag( "Java" );

		Borrower book7Borrower1 = new Borrower( "Juhan Juurikas" );
		book7Borrower1
			.setFrom( "2014-03-08T07:20:12Z" )
			.setTo( "2014-01-27T14:17:24Z" );
		book7.addBorrower( book7Borrower1 );

		Borrower book7Borrower2 = new Borrower( "Juhan Juurikas" );
		book7Borrower2
			.setFrom( "2014-01-25T14:17:24Z" )
			.setTo( "2014-02-04T14:17:24Z" );
		book7.addBorrower( book7Borrower2 );

		Comment book7Comment1 = new Comment( "Fantastic book!" );
		book7Comment1
			.setAuthor( "Juhan Juurikas" )
			.setRating( 10 )
			.setDate( "2014-02-15T14:17:24Z" );
		book7.addComment( book7Comment1 );
		this.books.add( book7 );

		// @todo Book8
		Book book8 = new Book( 8 );

		book8
			.setTitle( "Effective Unit Testing" )
			.setQuantity( 1 )
			.setAvailable( 0 )
			.setRating( 1 )
			.setLocation( "Tartu" )
			.setPublishingYear( 2010 )

			.addAuthor( "Lasse Koskela" )

			.addTag( "Testing" )
			.addTag( "JUnit" )
			.addTag( "Java" );

		Borrower book8Borrower1 = new Borrower( "Pille Kadakas" );
		book8Borrower1
			.setFrom( "2014-01-30T07:20:12Z" )
			.setTo( "2014-02-09T07:20:12Z" );
		book8.addBorrower( book8Borrower1 );

		Comment book8Comment1 = new Comment( "Too boring book!" );
		book8Comment1
			.setAuthor( "Pille Kadakas" )
			.setRating( 2 )
			.setDate( "2014-02-15T14:17:24Z" );
		book8.addComment( book8Comment1 );
		this.books.add( book8 );

		// @todo Book9
		Book book9 = new Book( 9 );

		book9
			.setTitle( "Enterprise OSGi in Action" )
			.setQuantity( 1 )
			.setAvailable( 0 )
			.setRating( 6 )
			.setLocation( "Tallinn" )
			.setPublishingYear( 2010 )

			.addAuthor( "Holly Cummins" )
			.addAuthor( "Timothy Ward" )

			.addTag( "OSGi" )
			.addTag( "Modularity" )
			.addTag( "Java" );

		Borrower book9Borrower1 = new Borrower( "Pille Kadakas" );
		book9Borrower1
			.setFrom( "2014-01-27T07:20:12Z" )
			.setTo( "2014-02-06T07:20:12Z" );
		book9.addBorrower( book9Borrower1 );

		Borrower book9Borrower2 = new Borrower( "Juhan Juurikas" );
		book9Borrower2
			.setFrom( "2014-02-04T14:17:24Z" )
			.setTo( "2014-02-14T14:17:24Z" );
		book9.addBorrower( book9Borrower2 );
		this.books.add( book9 );

		// @todo Book10
		Book book10 = new Book( 10 );

		book10
			.setTitle( "Gradle in Action" )
			.setQuantity( 3 )
			.setAvailable( 1 )
			.setRating( 10 )
			.setLocation( "Tartu" )
			.setPublishingYear( 2002 )

			.addAuthor( "Benjamin Muschko" )

			.addTag( "Gradle" )
			.addTag( "Java" );

		Borrower book10Borrower1 = new Borrower( "Pille Kadakas" );
		book10Borrower1
			.setFrom( "2014-01-22T14:17:24Z" )
			.setTo( "2014-02-01T14:17:24Z" );
		book10.addBorrower( book10Borrower1 );

		Borrower book10Borrower2 = new Borrower( "Juhan Juurikas" );
		book10Borrower2
			.setFrom( "2014-01-31T07:20:12Z" )
			.setTo( "2014-02-10T07:20:12Z" );
		book10.addBorrower( book10Borrower2 );

		Comment book10Comment1 = new Comment( "Fantastic book!" );
		book10Comment1
			.setAuthor( "Juhan Juurikas" )
			.setRating( 9 )
			.setDate( "2014-02-15T14:17:24Z" );
		book10.addComment( book10Comment1 );
		this.books.add( book10 );

		// @todo Book11
		Book book11 = new Book( 11 );

		book11
			.setTitle( "IntelliJ IDEA in Action" )
			.setQuantity( 2 )
			.setAvailable( 0 )
			.setRating( 3 )
			.setLocation( "Tallinn" )
			.setPublishingYear( 2011 )

			.addAuthor( "Duane K. Fields" )
			.addAuthor( "Stephen Saunders" )
			.addAuthor( "Eugene Belyaev" )

			.addTag( "IntelliJ" )
			.addTag( "IDE" )
			.addTag( "Java" );

		Borrower book11Borrower1 = new Borrower( "Pille Kadakas" );
		book11Borrower1
			.setFrom( "2014-03-08T07:20:12Z" )
			.setTo( "2014-01-27T14:17:24Z" );
		book11.addBorrower( book11Borrower1 );
		this.books.add( book11 );
	}
}