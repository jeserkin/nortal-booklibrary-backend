package com.nortal.exceptions;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

public class BookNotFoundException extends WebApplicationException
{
    public BookNotFoundException( final int bookId ) {
        super(
            Response
                .status( Status.NOT_FOUND )
                .entity( "Book not found: " + bookId )
                .build()
        );
    }
}