package com.nortal.rs;

import com.nortal.model.Book;
import com.nortal.services.BooksService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Collection;

@Path("/books")
public class BooksRestService
{
	@Inject
	private BooksService booksService;

	@Produces( { MediaType.APPLICATION_JSON } )
	@GET
	public Collection<Book> getBooks()
	{
		return booksService.getBooks();
	}

	@Produces( { MediaType.APPLICATION_JSON } )
	@Path( "/{bookId}" )
	@GET
	public Book getBookById( @PathParam( "bookId" ) final int bookId )
	{
		return booksService.getByBookId( bookId );
	}
}