package com.nortal.model;

public class Comment
{
    private String author;
    private int rating;
    private String date;
    private String content;

    public Comment()
    {
    }

    public Comment( String content )
    {
        this.content = content;
    }

    public Comment setAuthor( String author )
    {
        this.author = author;

        return this;
    }

    public String getAuthor()
    {
        return this.author;
    }

    public Comment setRating( int rating )
    {
        this.rating = rating;

        return this;
    }

    public int getRating()
    {
        return this.rating;
    }

    public Comment setDate( String date )
    {
        this.date = date;

        return this;
    }

    public String getDate()
    {
        return this.date;
    }

    public Comment setContent( String content )
    {
        this.content = content;

        return this;
    }

    public String getContent()
    {
        return this.content;
    }
}
