package com.nortal.model;

import java.util.ArrayList;

public class Book
{
    private int id;
    private String title;
    private ArrayList<String> authors;
    private ArrayList<String> tags;
	private String location;
    private int quantity;
    private int available;
    private int rating;
    private ArrayList<Borrower> borrowers;
    private ArrayList<Comment> comments;
	private int publishingYear;

    public Book()
    {
    }

    public Book( int id )
    {
        this.id = id;
    }

    public Book setId( int id )
    {
        this.id = id;

        return this;
    }

    public int getId()
    {
        return this.id;
    }

    public Book setTitle( String title )
    {
        this.title = title;

        return this;
    }

    public String getTitle()
    {
        return this.title;
    }

    public Book setAuthors( ArrayList<String> authors )
    {
        this.authors = authors;

        return this;
    }

	public Book addAuthor( String author )
	{
		if ( null == this.authors )
		{
			this.authors = new ArrayList<String>();
		}

		this.authors.add( author );

		return this;
	}

    public ArrayList<String> getAuthors()
    {
        return this.authors;
    }

    public Book setTags( ArrayList<String> tags )
    {
        this.tags = tags;

        return this;
    }

	public Book addTag( String tag )
	{
		if ( null == this.tags )
		{
			this.tags = new ArrayList<String>();
		}

		this.tags.add( tag );

		return this;
	}

    public ArrayList<String> getTags()
    {
        return this.tags;
    }

	public Book setLocation( String location )
	{
		this.location = location;

		return this;
	}

	public String getLocation()
	{
		return this.location;
	}

    public Book setQuantity( int quantity )
    {
        this.quantity = quantity;

        return this;
    }

    public int getQuantity()
    {
        return this.quantity;
    }

    public Book setAvailable( int available )
    {
        this.available = available;

        return this;
    }

    public int getAvailable()
    {
        return this.available;
    }

    public Book setRating( int rating )
    {
        this.rating = rating;

        return this;
    }

    public int getRating()
    {
        return this.rating;
    }

    public Book setBorrowers( ArrayList<Borrower> borrowers )
    {
        this.borrowers = borrowers;

        return this;
    }

	public Book addBorrower( Borrower borrower )
	{
		if ( null == this.borrowers )
		{
			this.borrowers = new ArrayList<Borrower>();
		}

		this.borrowers.add( borrower );

		return this;
	}

    public ArrayList<Borrower> getBorrowers()
    {
        return this.borrowers;
    }

    public Book setComments( ArrayList<Comment> coments )
    {
        this.comments = coments;

        return this;
    }

	public Book addComment( Comment coment )
	{
		if ( null == this.comments )
		{
			this.comments = new ArrayList<Comment>();
		}

		this.comments.add( coment );

		return this;
	}

    public ArrayList<Comment> getComments()
    {
        return this.comments;
    }

	public Book setPublishingYear( int publishingYear )
	{
		this.publishingYear = publishingYear;

		return this;
	}

	public int getPublishingYear()
	{
		return this.publishingYear;
	}
}