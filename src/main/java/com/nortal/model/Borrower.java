package com.nortal.model;

public class Borrower
{
    private String name;
    private String from;
    private String to;

    public Borrower()
    {
    }

    public Borrower( String name )
    {
        this.name = name;
    }

    public Borrower setName( String name )
    {
        this.name = name;

        return this;
    }

    public String getName()
    {
        return this.name;
    }

    public Borrower setFrom( String from )
    {
        this.from = from;

        return this;
    }

    public String getFrom()
    {
        return this.from;
    }

    public Borrower setTo( String to )
    {
        this.to = to;

        return this;
    }

    public String getTo()
    {
        return this.to;
    }
}
