package com.nortal;

import com.nortal.config.AppConfig;
import com.nortal.rs.SimpleCORSFilter;
import org.apache.cxf.transport.servlet.CXFServlet;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

import javax.servlet.DispatcherType;
import java.util.EnumSet;

public class Starter
{
	public static void main( String[] args ) throws Exception
	{
		Server server = new Server( 8080 );

		// Register and map the dispatcher servlet
		final ServletHolder servletHolder = new ServletHolder( new CXFServlet() );
		final ServletContextHandler context = new ServletContextHandler();

		context.setContextPath( "/" );
		context.addServlet( servletHolder, "/nortal-library/*" );
		context.addEventListener( new ContextLoaderListener() );

		context.setInitParameter( "contextClass", AnnotationConfigWebApplicationContext.class.getName() );
		context.setInitParameter( "contextConfigLocation", AppConfig.class.getName() );
		context.addFilter( SimpleCORSFilter.class, "/*", EnumSet.of(DispatcherType.REQUEST) );

		server.setHandler( context );
		server.start();
		server.join();
	}
}